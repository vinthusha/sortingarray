package bcas.dsa.sorting;

public class SortAscending {
	public static void printbeforeSort(int[] numArr) {

		System.out.println("Before Sorting Array: ");
		for (int i = 0; i < numArr.length; i++) {
			System.out.print(numArr[i] + " ");
		}

		System.out.println();

	}

	public static int[] sorting(int[] numArr) {
		int cup = 0;
		for (int i = 0; i < numArr.length; i++) {
			for (int j = i + 1; j < numArr.length; j++) {
				if (numArr[i] > numArr[j]) {
					cup = numArr[i];
					numArr[i] = numArr[j];
					numArr[j] = cup;
				}
			}
		}
		return numArr;

	}

	public static void printAfterSort(int[] numArr) {
		System.out.println("After Sorting Array: ");
		for (int i = 0; i < numArr.length; i++) {
			System.out.print(numArr[i] + " ");
		}

	}

}
