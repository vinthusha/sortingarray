package bcas.dsa.sorting;

public class SortAscendingDemo {

	public static void main(String[] args) {

		int[] numArr = new int[] { 10, 5, 8, 9, 1 };
		SortAscending.printbeforeSort(numArr);
		SortAscending.printAfterSort(SortAscending.sorting(numArr));

	}
}
