package bcas.dsa.sorting;

public class SortedArrayDemo {
	public static void main(String[] args) {
		int[] myArray = { 4, 6, 7, 3, 8, 9 };
		SortedArray sortarray = new SortedArray();
		System.out.println(sortarray.min(myArray));
		System.out.println(sortarray.findIndex(myArray));
	}
}
